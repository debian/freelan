freelan (2.2-3) unstable; urgency=medium

  * QA Upload.
  * Update Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no
  * Add folder to d/clean
  * Set upstream metadata fields: Bug-Database, Repository,
    Repository-Browse, Bug-Submit.
  * Add upstream commit as 0007-Move-to-Python-3.patch
  * Add upstream commit as 0006-Fix-build-outside-git.patch closes: #952306

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sat, 29 Feb 2020 15:56:31 +0100

freelan (2.2-2) unstable; urgency=medium

  * QA upload
  * Use nproc to parallelize the build
  * Update manpage
  * Change configuration filename from .conf to .cfg in init.d script
  * Freelan binary patch only changes binary path
  * Fix paths that's being exported to scons

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sat, 18 Jan 2020 20:37:52 +0100

freelan (2.2-1) unstable; urgency=medium

  * QA upload
  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one

  [ Håvard Flaget Aasen ]
  * New upstream release
    - Upstream fixes ftbfs with boost 1.67 closes: #914068
    - By default it compiles with upnp=yes and moongoose=no
  * Create debian/watch file
  * d/control
    - Added Pre-Depends: ${misc:Pre-Depends}
    - Change to debhelper-compat
    - Bump debhelper to 12
    - Update Standards-Version to 4.4.1
    - Use secure URI on homepage
    - Add libminiupnpc-dev as build-dependency
  * Updated d/clean
  * d/rules
    - Updated install paths
  * Update paths in d/copyright, upstream has restructured the code.
  * Patches
    - All but two patches got removed, fixed upstream.
    - Add 0001-build-Adds-support-for-Boost-1.70.patch
    - Add 0002-Fix-for-boost-versions-less-than-1.66.patch

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 12 Jan 2020 21:39:01 +0100

freelan (2.0-8) unstable; urgency=medium

  * QA upload.
  * Move freelan binary to /usr/sbin.  (Closes: #834712)
  * Do not build with -Werror.  (Closes: #897753)
  * Bump Standards-Version to 4.1.5, no changes needed.
  * Fix building twice in a row by cleaning more build artefacts.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 08 Jul 2018 11:51:09 +0200

freelan (2.0-7) unstable; urgency=medium

  * QA upload.
  * Add d/gbp.conf to deal with the non-standard repository layout.
  * d/control:
    + Remove Uploaders field as the package has been orphaned.
    + wrap-and-sort.
    + Move Vcs-* fields to salsa.debian.org.
    + Bump Standards-Version to 4.1.4:
      - Replace Priority:extra (deprecated) with optional.
      - d/copyright: Use HTTPS in the Format field.
  * Bump debhelper compat level to 11.
  * Add patch from Ubuntu to fix build with boost 1.65.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 27 May 2018 22:41:18 +0200

freelan (2.0-6) unstable; urgency=medium

  * acknowledge previous NMU.  Thanks, Adrian!
  * orphan the package
    I can unfortunately no longer justify the effort to package for Debian

 -- Rolf Leggewie <foss@rolf.leggewie.biz>  Sat, 21 Apr 2018 23:14:07 +0800

freelan (2.0-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream fix for FTBFS with glibc 2.25. (Closes: #882733)

 -- Adrian Bunk <bunk@debian.org>  Wed, 27 Dec 2017 22:18:47 +0200

freelan (2.0-5) unstable; urgency=medium

  [ Sebastien Vincent ]
  * patches: backport 44087faf to re-enable compilation with OpenSSL 1.0.x

  [ Rolf Leggewie ]
  * add README.Debian with instructions on simple test setup

 -- Rolf Leggewie <foss@rolf.leggewie.biz>  Tue, 05 Sep 2017 21:29:41 +0800

freelan (2.0-4) unstable; urgency=medium

  [ Sebastien Vincent ]
  * patches: backport 5014a8023b from upstream. Closes: #853406
    fix FTBFS with gcc7

  [ Rolf Leggewie ]
  * control: update to standards version 4.0.0
    - fix typo in manpage
    - make the whatis entry for the freelan manpage more meaningful
  * copyright: update my copyright to 2017

 -- Rolf Leggewie <foss@rolf.leggewie.biz>  Sun, 06 Aug 2017 17:57:54 +0800

freelan (2.0-2) unstable; urgency=medium

  [ Julien Kauffmann ]
  * patches: backport changes to compile with openssl 1.1. Closes: #828304

  [ Rolf Leggewie ]
  * control:
    - add explicit run-time dependency on lsb-base
    - bump standards version to 3.9.8
  * copyright: update my copyright to 2016

 -- Rolf Leggewie <foss@rolf.leggewie.biz>  Tue, 15 Nov 2016 14:19:18 +0800

freelan (2.0-1) unstable; urgency=low

  * initial release to Debian. Closes: #780608
    based on previous work by Julien Kauffmann and Etienne Champetier

 -- Rolf Leggewie <foss@rolf.leggewie.biz>  Tue, 05 Jan 2016 23:48:09 +0800
